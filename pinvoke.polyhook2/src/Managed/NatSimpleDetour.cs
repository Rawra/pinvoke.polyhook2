﻿using PInvoke.PolyHook2.Enums;
using System;
using System.Runtime.InteropServices;
namespace PInvoke.PolyHook2.Managed
{
    /// <summary>
    /// Simple Detour managed class wrapper
    /// 
    /// Use this class if you want to avoid delegates directly, and would like
    /// to work with function pointers only.
    /// </summary>
    /// <typeparam name="T">The function signature delegate</typeparam>
    public class NatSimpleDetour : SafeHandle, ISimpleDetour
    {
        /// <summary>
        /// The function address we are hooking
        /// </summary>
        public UInt64 FunctionAddress;

        /// <summary>
        /// The function callback address
        /// </summary>
        public UInt64 FunctionCallbackAddress;

        /// <summary>
        /// The function trampline address
        /// </summary>
        public UInt64 TrampolineAddress;

        public override bool IsInvalid => handle == IntPtr.Zero;

        public NatSimpleDetour(UInt64 fnAddress, UInt64 fnCallbackAddress, bool doAutoHook = true) : base(IntPtr.Zero, ownsHandle: true)
        {
            this.FunctionAddress = fnAddress;
            this.FunctionCallbackAddress = fnCallbackAddress;
            this.TrampolineAddress = 0;

            if (Environment.Is64BitProcess)
            {
                handle = PInvokeInterface.ph2_x64Detour_create(FunctionAddress, FunctionCallbackAddress, out TrampolineAddress);
            } 
            else
            {
                handle = PInvokeInterface.ph2_x86Detour_create(FunctionAddress, FunctionCallbackAddress, out TrampolineAddress);
            }
            if (handle == IntPtr.Zero)
                throw new NullReferenceException($"Error creating detour at {FunctionAddress.ToString("X")}, cb={FunctionCallbackAddress.ToString("X")}");

            if (doAutoHook)
                Hook();
        }

        /// <summary>
        /// Order the underlying existing detour to hook.
        /// </summary>
        /// <returns>Success</returns>
        public virtual bool Hook()
        {
            if (handle == IntPtr.Zero)
                return false;

            if (Environment.Is64BitProcess)
            {
                return PInvokeInterface.ph2_x64Detour_hook(handle);
            }
            return PInvokeInterface.ph2_x86Detour_hook(handle);
        }

        /// <summary>
        /// Order the underlying existing detour to unhook.
        /// </summary>
        /// <returns>Success</returns>
        public virtual bool UnHook()
        {
            if (handle == IntPtr.Zero)
                return false;
            if (Environment.Is64BitProcess)
            {
                return PInvokeInterface.ph2_Detour_unHook(handle);
            }
            return PInvokeInterface.ph2_Detour_unHook(handle);
        }

        /// <summary>
        /// Order the underlying existing detour to re-hook.
        /// </summary>
        /// <returns>Success</returns>
        public virtual bool ReHook()
        {
            if (handle == IntPtr.Zero)
                return false;

            if (Environment.Is64BitProcess)
            {
                return PInvokeInterface.ph2_Detour_reHook(handle);
            }
            return PInvokeInterface.ph2_IHook_reHook(handle);
        }

        /// <summary>
        /// Check if the underlying detour is actively hooked.
        /// </summary>
        /// <returns></returns>
        public virtual bool IsHooked()
        {
            if (handle == IntPtr.Zero)
                return false;

            if (Environment.Is64BitProcess)
            {
                return PInvokeInterface.ph2_IHook_isHooked(handle);
            }
            return PInvokeInterface.ph2_IHook_isHooked(handle);
        }

        /// <summary>
        /// Get the hook architecture
        /// </summary>
        /// <returns></returns>
        public virtual Mode GetHookType()
        {
            if (Environment.Is64BitProcess)
            {
                return PInvokeInterface.ph2_x64Detour_getArchType(handle);
            }
            return PInvokeInterface.ph2_x86Detour_getArchType(handle);
        }

        protected override bool ReleaseHandle()
        {
            if (Environment.Is64BitProcess)
            {
                PInvokeInterface.ph2_x64Detour_destructor(handle);
                return true;
            }
            PInvokeInterface.ph2_x86Detour_destructor(handle);
            return true;
        }
    }
}
