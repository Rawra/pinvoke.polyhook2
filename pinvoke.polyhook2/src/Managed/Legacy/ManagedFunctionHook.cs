﻿using PInvoke.PolyHook2.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PInvoke.PolyHook2
{
    /// <summary>
    /// Abstract managed function hook.
    /// More specified hooks derive from this class.
    /// 
    /// Use this if you want a simple class to help you hook functions
    /// in combination with delegates where needed.
    /// </summary>
    /// <typeparam name="T">Function signature delegate</typeparam>
    public abstract class ManagedFunctionHook<T> where T : Delegate
    {
        public UInt64 fnAddress;
        public UInt64 fnCallbackAddress;

        public T? Trampoline;
        public UInt64 fnTrampolineAddress;

        protected IntPtr handle;

        protected bool isHooked;
        protected bool doAutoHook;

        /// <summary>
        /// Initialize the hook itself
        /// </summary>
        public abstract void Initialize();

        // Detour API
        public virtual void Destructor()
        {
            PInvokeInterface.ph2_Detour_destructor(handle);
        }
        public virtual Mode GetArchType()
        {
            return PInvokeInterface.ph2_Detour_getArchType(handle);
        }
        public virtual byte GetMaxDepth()
        {
            return PInvokeInterface.ph2_Detour_getMaxDepth(handle);
        }
        public virtual void SetMaxDepth(byte depth)
        {
            PInvokeInterface.ph2_Detour_setMaxDepth(handle, depth);
        }
        public virtual HookType GetHookType()
        {
            return PInvokeInterface.ph2_IHook_getType(handle);
        }
        public void SetIsFollowCallOnFnAddress(bool value)
        {
            PInvokeInterface.ph2_Detour_setIsFollowCallOnFnAddress(handle, value);
        }

        // IHook API
        /// <summary>
        /// Proceed to activate the hook.
        /// </summary>
        /// <returns>Success</returns>
        public abstract bool Hook();

        /// <summary>
        /// Unhook any given hook.
        /// </summary>
        /// <returns>Success</returns>
        /// <exception cref="Exception"></exception>
        public virtual bool UnHook()
        {
            if (handle == IntPtr.Zero)
                throw new Exception("ManagedFunctionHook: polyHookDetourHandle is null");

            if (!PInvokeInterface.ph2_Detour_unHook(handle))
                return false;

            if (fnTrampolineAddress == 0)
                return false;

            Trampoline = null;
            return true;
        }

        public virtual bool ReHook()
        {
            if (handle == IntPtr.Zero)
                throw new Exception("ManagedFunctionHook: polyHookDetourHandle is null");

            return PInvokeInterface.ph2_IHook_reHook(handle);
        }

        public virtual bool IsHooked()
        {
            return PInvokeInterface.ph2_IHook_isHooked(handle);
        }
        
        public IntPtr GetDetourHandle()
        {
            return handle;
        }
    }
}
