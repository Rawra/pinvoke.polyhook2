﻿using PInvoke.PolyHook2.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PInvoke.PolyHook2
{
    /// <summary>
    /// Managed function hook for x86_64 native function
    /// </summary>
    /// <typeparam name="T">Function signature delegate</typeparam>
    public class X64ManagedFunctionHook<T> : ManagedFunctionHook<T>, IDisposable where T : Delegate
    {
        /// <summary>
        /// Create the hook for the function at specified address, with a callback at a given delegate.
        /// </summary>
        /// <param name="fnAddress">Native function address</param>
        /// <param name="fnCallback">Managed function callback</param>
        /// <param name="doAutoHook">To immediately hook or not</param>
        public X64ManagedFunctionHook(UInt64 fnAddress, T fnCallback, bool doAutoHook = true)
        {
            this.doAutoHook = doAutoHook;
            this.isHooked = false;
            this.fnAddress = fnAddress;
            fnCallbackAddress = (UInt64)Marshal.GetFunctionPointerForDelegate<T>(fnCallback);

            Initialize();
        }

        public override void Initialize()
        {
            IntPtr fnAddressLog = (IntPtr)(Int64)(UIntPtr)(Int64)fnAddress;
            IntPtr fnCallbackAddressLog = (IntPtr)(Int64)(UIntPtr)(Int64)fnCallbackAddress;
            //Console.WriteLine($"X64ManagedFunctionHook.Initialize: fnAddressLog: {fnAddressLog.ToString("X16")}");
            //Console.WriteLine($"X64ManagedFunctionHook.Initialize: fnCallbackAddressLog: {fnCallbackAddressLog.ToString("X16")}");

            handle = PInvokeInterface.ph2_x64Detour_create(fnAddress, fnCallbackAddress, out fnTrampolineAddress);
            if (handle == IntPtr.Zero)
                throw new Exception("X64ManagedFunctionHook: polyHookDetourHandle is nullptr");

            if (this.doAutoHook)
                this.isHooked = Hook();
        }

        public override bool Hook()
        {
            //Console.WriteLine($"ph2_x64Detour: {polyHookDetourHandle.ToString("X16")}");
            if (handle == IntPtr.Zero)
                throw new Exception("X64ManagedFunctionHook: ph2_x64Detour is nullptr");

            // note: no clue why we have to do it like this, Handle gets set to 0 upon calling hook otherwise here.
            IntPtr handleBackup = handle;
            if (!PInvokeInterface.ph2_x64Detour_hook(handle))
                throw new Exception("X64ManagedFunctionHook: ph2_x64Detour_hook failed");
            handle = handleBackup;

            IntPtr fnTrampolineAddressLog = (IntPtr)(Int64)(UIntPtr)(Int64)fnTrampolineAddress;
            //Console.WriteLine($"X64ManagedFunctionHook.Initialize: fnTrampolineAddress: {fnTrampolineAddressLog.ToString("X16")}");
            if (fnTrampolineAddress == 0)
                return false;

            Trampoline = Marshal.GetDelegateForFunctionPointer<T>((IntPtr)(Int64)fnTrampolineAddress);
            isHooked = true;
            return true;
        }

        public override void Destructor()
        {
            PInvokeInterface.ph2_x64Detour_destructor(handle);
        }

        public override Mode GetArchType()
        {
            return PInvokeInterface.ph2_x64Detour_getArchType(handle);
        }

        public virtual void SetDetourScheme(DetourScheme scheme)
        {
            PInvokeInterface.ph2_x64Detour_setDetourScheme(handle, scheme);
        }

        public virtual DetourScheme GetDetourScheme()
        {
            return PInvokeInterface.ph2_x64Detour_getDetourScheme(handle);
        }

        public static byte GetMinJmpSize()
        {
            return PInvokeInterface.ph2_x64Detour_getMinJmpSize();
        }

        public void Dispose()
        {
            if (handle != IntPtr.Zero)
            {
                UnHook();
                Destructor();
                handle = IntPtr.Zero;
            }
        }
    }
}
