﻿using PInvoke.PolyHook2.Enums;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PInvoke.PolyHook2
{
    /// <summary>
    /// Managed function hook for x86/x86_64 native function (depending on build version)
    /// </summary>
    /// <typeparam name="T">Function signature delegate</typeparam>
    public class NatManagedFunctionHook<T> : ManagedFunctionHook<T>, IDisposable where T : Delegate
    {
        /// <summary>
        /// Create the hook for the function at specified address, with a callback at a given delegate.
        /// </summary>
        /// <param name="fnAddress">Native function address</param>
        /// <param name="fnCallback">Managed function callback</param>
        /// <param name="doAutoHook">To immediately hook or not</param>
        public NatManagedFunctionHook(UInt64 fnAddress, T fnCallback, bool doAutoHook = true)
        {
            this.doAutoHook = doAutoHook;
            this.isHooked = false;
            this.fnAddress = fnAddress;
            fnCallbackAddress = (UInt64)Marshal.GetFunctionPointerForDelegate<T>(fnCallback);

            Initialize();
        }

        public override void Initialize()
        {
            handle = PInvokeInterface.ph2_natDetour_create(fnAddress, fnCallbackAddress, out fnTrampolineAddress);
            if (handle == IntPtr.Zero)
                throw new Exception("NatManagedFunctionHook: polyHookDetourHandle is nullptr");

            if (this.doAutoHook)
                this.isHooked = Hook();
        }

        /// <summary>
        /// The hook architecture will depend on the hosting process.
        /// </summary>
        /// <returns>success</returns>
        /// <exception cref="Exception"></exception>
        public override bool Hook()
        {
            //Console.WriteLine($"ph2_natDetour: {polyHookDetourHandle.ToString("X16")}");
            if (handle == IntPtr.Zero)
                throw new Exception("NatManagedFunctionHook: ph2_natDetour is nullptr");

            // note: no clue why we have to do it like this, Handle gets set to 0 upon calling hook otherwise here.
            IntPtr handleBackup = handle;
            if (Environment.Is64BitProcess)
            {
                if (!PInvokeInterface.ph2_x64Detour_hook(handle))
                    throw new Exception("NatManagedFunctionHook: ph2_x64Detour_hook failed");
            } 
            else
            {
                if (!PInvokeInterface.ph2_x86Detour_hook(handle))
                    throw new Exception("NatManagedFunctionHook: ph2_x86Detour_hook failed");
            }
            handle = handleBackup;

            IntPtr fnTrampolineAddressLog = (IntPtr)(Int64)(UIntPtr)(Int64)fnTrampolineAddress;
            //Console.WriteLine($"NatManagedFunctionHook.Initialize: fnTrampolineAddress: {fnTrampolineAddressLog.ToString("X16")}");
            if (fnTrampolineAddress == 0)
                return false;

            Trampoline = Marshal.GetDelegateForFunctionPointer<T>((IntPtr)(Int64)fnTrampolineAddress);
            isHooked = true;
            return true;
        }

        public override void Destructor()
        {
            if (Environment.Is64BitProcess)
            {
                PInvokeInterface.ph2_x64Detour_destructor(handle);
            } 
            else
            {
                PInvokeInterface.ph2_x64Detour_destructor(handle);
            }
        }

        public override Mode GetArchType()
        {
            if (Environment.Is64BitProcess)
            {
                return PInvokeInterface.ph2_x64Detour_getArchType(handle);
            } 
            else
            {
                return PInvokeInterface.ph2_x86Detour_getArchType(handle);
            }
        }

        public void Dispose()
        {
            if (handle != IntPtr.Zero)
            {
                UnHook();
                Destructor();
                handle = IntPtr.Zero;
            }
        }
    }
}
