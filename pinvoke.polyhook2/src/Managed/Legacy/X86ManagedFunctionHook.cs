﻿using PInvoke.PolyHook2.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PInvoke.PolyHook2
{
    /// <summary>
    /// Managed function hook for x86 native function
    /// </summary>
    /// <typeparam name="T">Function signature delegate</typeparam>
    public class X86ManagedFunctionHook<T> : ManagedFunctionHook<T>, IDisposable where T : Delegate
    {
        /// <summary>
        /// Create the hook for the function at specified address, with a callback at a given delegate.
        /// </summary>
        /// <param name="fnAddress">Native function address</param>
        /// <param name="fnCallback">Managed function callback</param>
        /// <param name="doAutoHook">To immediately hook or not</param>
        public X86ManagedFunctionHook(UInt64 fnAddress, T fnCallback, bool doAutoHook = true)
        {
            this.doAutoHook = doAutoHook;
            this.isHooked = false;
            this.fnAddress = fnAddress;
            fnCallbackAddress = (UInt64)Marshal.GetFunctionPointerForDelegate<T>(fnCallback);

            Initialize();
        }

        public override void Initialize()
        {
            handle = PInvokeInterface.ph2_x86Detour_create(fnAddress, fnCallbackAddress, out fnTrampolineAddress);
            if (handle == IntPtr.Zero)
                throw new Exception("X86ManagedFunctionHook: polyHookDetourHandle is nullptr");

            if (this.doAutoHook)
                this.isHooked = Hook();
        }

        public override bool Hook()
        {
            //Console.WriteLine($"ph2_x86Detour: {polyHookDetourHandle.ToString("X8")}");
            if (handle == IntPtr.Zero)
                throw new Exception("X86ManagedFunctionHook: ph2_x86Detour is nullptr");


            // note: no clue why we have to do it like this, Handle gets set to 0 upon calling hook otherwise here.
            IntPtr handleBackup = handle;
            if (!PInvokeInterface.ph2_x86Detour_hook(handle))
                throw new Exception("X86ManagedFunctionHook: ph2_x86Detour_hook failed");
            handle = handleBackup;

            if (fnTrampolineAddress == 0)
                return false;

            Trampoline = Marshal.GetDelegateForFunctionPointer<T>((IntPtr)(int)fnTrampolineAddress);
            isHooked = true;
            return true;
        }

        public override void Destructor()
        {
            PInvokeInterface.ph2_x86Detour_destructor(handle);
        }

        public override Mode GetArchType()
        {
            return PInvokeInterface.ph2_x86Detour_getArchType(handle);
        }

        public void Dispose()
        {
            if (handle != IntPtr.Zero)
            {
                UnHook();
                Destructor();
                handle = IntPtr.Zero;
            }
        }
    }
}
