﻿using PInvoke.PolyHook2.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PInvoke.PolyHook2.Managed
{
    public interface ISimpleDetour
    {
        public bool Hook();
        public bool UnHook();
        public bool ReHook();
        public bool IsHooked();
        public Mode GetHookType();
    }
}
