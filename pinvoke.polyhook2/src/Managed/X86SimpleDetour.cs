﻿using PInvoke.PolyHook2.Enums;
using System;
using System.Runtime.InteropServices;
namespace PInvoke.PolyHook2.Managed
{
    /// <summary>
    /// Simple Detour managed class wrapper
    /// 
    /// Use this class if you want to avoid delegates directly, and would like
    /// to work with function pointers only.
    /// </summary>
    /// <typeparam name="T">The function signature delegate</typeparam>
    public class X86SimpleDetour : SafeHandle, ISimpleDetour
    {
        /// <summary>
        /// The function address we are hooking
        /// </summary>
        public UInt64 FunctionAddress;

        /// <summary>
        /// The function callback address
        /// </summary>
        public UInt64 FunctionCallbackAddress;

        /// <summary>
        /// The function trampline address
        /// </summary>
        public UInt64 TrampolineAddress;

        public override bool IsInvalid => handle == IntPtr.Zero;

        public X86SimpleDetour(UInt64 fnAddress, UInt64 fnCallbackAddress, bool doAutoHook = true) : base(IntPtr.Zero, ownsHandle: true)
        {
            this.FunctionAddress = fnAddress;
            this.FunctionCallbackAddress = fnCallbackAddress;
            this.TrampolineAddress = 0;
            handle = PInvokeInterface.ph2_x86Detour_create(FunctionAddress, FunctionCallbackAddress, out TrampolineAddress);
            if (handle == IntPtr.Zero)
                throw new NullReferenceException($"Error creating detour at {FunctionAddress.ToString("X")}, cb={FunctionCallbackAddress.ToString("X")}");

            if (doAutoHook)
                Hook();
        }

        /// <summary>
        /// Order the underlying existing detour to hook.
        /// </summary>
        /// <returns>Success</returns>
        public virtual bool Hook()
        {
            if (handle == IntPtr.Zero)
                return false;

            return PInvokeInterface.ph2_x86Detour_hook(handle);
        }

        /// <summary>
        /// Order the underlying existing detour to unhook.
        /// </summary>
        /// <returns>Success</returns>
        public virtual bool UnHook()
        {
            if (handle == IntPtr.Zero)
                return false;

            return PInvokeInterface.ph2_Detour_unHook(handle);
        }

        /// <summary>
        /// Order the underlying existing detour to re-hook.
        /// </summary>
        /// <returns>Success</returns>
        public virtual bool ReHook()
        {
            if (handle == IntPtr.Zero)
                return false;

            return PInvokeInterface.ph2_IHook_reHook(handle);
        }

        /// <summary>
        /// Check if the underlying detour is actively hooked.
        /// </summary>
        /// <returns></returns>
        public virtual bool IsHooked()
        {
            if (handle == IntPtr.Zero)
                return false;

            return PInvokeInterface.ph2_IHook_isHooked(handle);
        }

        /// <summary>
        /// Get the hook architecture
        /// </summary>
        /// <returns></returns>
        public virtual Mode GetHookType()
        {
            return Mode.x86;
        }

        protected override bool ReleaseHandle()
        {
            PInvokeInterface.ph2_x86Detour_destructor(handle);
            return true;
        }
    }
}
