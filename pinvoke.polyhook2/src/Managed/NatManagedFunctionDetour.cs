﻿using PInvoke.PolyHook2.Managed;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PInvoke.PolyHook2
{
    public class NatManagedFunctionDetour<T> : NatSimpleDetour, ISimpleDetour where T : Delegate
    {

        public NatManagedFunctionDetour(UInt64 fnAddress, UInt64 fnCallbackAddress, bool doAutoHook = true) : base(fnAddress, fnCallbackAddress, doAutoHook: false)
        {
            if (doAutoHook)
            {
                Hook();
            }
        }
        public NatManagedFunctionDetour(UInt64 fnAddress, T fnCallback, bool doAutoHook = true) : base(fnAddress, (UInt64)Marshal.GetFunctionPointerForDelegate(fnCallback), doAutoHook: false)
        {
            if (doAutoHook)
            {
                Hook();
            }
        }
        public T? Trampoline;

        public override bool UnHook()
        {
            Trampoline = null;
            return base.UnHook();
        }

        public override bool ReHook()
        {
            if (!base.ReHook())
                return false;

            Trampoline = Marshal.GetDelegateForFunctionPointer<T>((IntPtr)TrampolineAddress);
            return true;
        }

        public override bool Hook()
        {
            if (!base.Hook())
                return false;

            Trampoline = Marshal.GetDelegateForFunctionPointer<T>((IntPtr)TrampolineAddress);
            return true;
        }
    }
}
