﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PInvoke.PolyHook2.Managed
{
    [StructLayout(LayoutKind.Explicit, Size = 0x04)]
    public unsafe struct Bool32 : IComparable, IConvertible, IComparable<Bool32>, IEquatable<Bool32>
    {
        [FieldOffset(0x00)] public byte Value;
        [FieldOffset(0x01)] public fixed byte padding[3];

        public bool GetValue()
        {
            return (Value > 0);
        }

        public void SetValue(bool value)
        {
            this.Value = value ? (byte)1 : (byte)0;
        }

        // Constructors
        public Bool32(bool value)
        {
            this.Value = value ? (byte)1 : (byte)0;
            padding[0] = 0;
            padding[1] = 0;
            padding[2] = 0;
        }

        public static Bool32 True()
        {
            return new Bool32(true);
        }

        public static Bool32 False()
        {
            return new Bool32(false);
        }

        public bool IsTrue() => GetValue() == true;

        // Comparison Overloads

        // bool32, bool32
        public static bool operator ==(Bool32 x, Bool32 y)
        {
            return x.Value == y.Value;
        }

        public static bool operator !=(Bool32 x, Bool32 y)
        {
            return x.Value != y.Value;
        }

        // bool32, managed bool
        public static bool operator ==(Bool32 x, bool y)
        {
            return x.GetValue() == y;
        }
        public static bool operator !=(Bool32 x, bool y)
        {
            return x.GetValue() != y;
        }

        // managed bool, bool32
        public static bool operator ==(bool x, Bool32 y)
        {
            return x == y.GetValue();
        }
        public static bool operator !=(bool x, Bool32 y)
        {
            return x != y.GetValue();
        }

        public bool Equals(Bool32 other)
        {
            return Value.Equals(other.Value);
        }

        // IEquatable
        public override bool Equals(object? obj)
        {
            return obj is Bool32 && Equals((Bool32)obj);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        // IComparable
        public int CompareTo(object? obj)
        {
            return (obj is Bool32) ? ((Bool32)obj).CompareTo(this) : 0;
        }

        // IConvertible
        public TypeCode GetTypeCode()
        {
            return TypeCode.Boolean;
        }

        public bool ToBoolean(IFormatProvider? provider)
        {
            return GetValue() ? true : false;
        }

        public byte ToByte(IFormatProvider? provider)
        {
            return Convert.ToByte(Value, provider);
        }

        public char ToChar(IFormatProvider? provider)
        {
            return Convert.ToChar(Value, provider);
        }

        public DateTime ToDateTime(IFormatProvider? provider)
        {
            throw new InvalidCastException("Bool32 -> DateTime");
        }

        public decimal ToDecimal(IFormatProvider? provider)
        {
            return Convert.ToDecimal(Value, provider);
        }

        public double ToDouble(IFormatProvider? provider)
        {
            return Convert.ToDouble(Value, provider);
        }

        public short ToInt16(IFormatProvider? provider)
        {
            return Convert.ToInt16(Value, provider);
        }

        public int ToInt32(IFormatProvider? provider)
        {
            return Convert.ToInt32(Value, provider);
        }

        public long ToInt64(IFormatProvider? provider)
        {
            return Convert.ToInt64(Value, provider);
        }

        public sbyte ToSByte(IFormatProvider? provider)
        {
            return Convert.ToSByte(Value, provider);
        }

        public float ToSingle(IFormatProvider? provider)
        {
            return Convert.ToSingle(Value, provider);
        }

        public string ToString(IFormatProvider? provider)
        {
            return Convert.ToString(Value, provider);
        }

        public object ToType(Type type, IFormatProvider? provider)
        {
            throw new NotSupportedException("Bool32 ToType");
        }

        public ushort ToUInt16(IFormatProvider? provider)
        {
            return Convert.ToUInt16(Value, provider);
        }

        public uint ToUInt32(IFormatProvider? provider)
        {
            return Convert.ToUInt32(Value, provider);
        }

        public ulong ToUInt64(IFormatProvider? provider)
        {
            return Convert.ToUInt64(Value, provider);
        }

        // IComparable<Bool32>
        public int CompareTo(Bool32 other)
        {
            return Value.CompareTo(other.Value);
        }

        public static Bool32 Parse(string s, IFormatProvider? provider)
        {
            return new Bool32(bool.Parse(s));
        }
    }
}
