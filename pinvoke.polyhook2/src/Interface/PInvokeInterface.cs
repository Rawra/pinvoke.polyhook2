﻿using PInvoke.PolyHook2.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace PInvoke.PolyHook2
{
    [SuppressUnmanagedCodeSecurity]
    public static unsafe class PInvokeInterface
    {
        private const string DllName = "cpolyhook2.dll";

        // nat detour

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr ph2_natDetour_create(UInt64 fnAddress, UInt64 fnCallback, out UInt64 userTrampVar);

        // x86 detour
        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr ph2_x86Detour_create(UInt64 fnAddress, UInt64 fnCallback, out UInt64 userTrampVar);

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static extern bool ph2_x86Detour_hook(IntPtr self);

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern Mode ph2_x86Detour_getArchType(IntPtr self);

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern void ph2_x86Detour_destructor(IntPtr self);

        //
        // x64Detour Interface
        //

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr ph2_x64Detour_create(UInt64 fnAddress, UInt64 fnCallback, out UInt64 userTrampVar);

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool ph2_x64Detour_hook(IntPtr self);
        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern Mode ph2_x64Detour_getArchType(IntPtr self);

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern DetourScheme ph2_x64Detour_getDetourScheme(IntPtr self);

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern void ph2_x64Detour_setDetourScheme(IntPtr self, DetourScheme scheme);

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern byte ph2_x64Detour_getMinJmpSize();
        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.LPStr)]
        public static extern string ph2_x64Detour_printDetourScheme(DetourScheme scheme);

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern void ph2_x64Detour_destructor(IntPtr self);

        //
        // ADetour Interface
        //

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr ph2_Detour_new(UInt64 fnAddress, UInt64 fnCallback, out UInt64 userTrampVar, Mode mode);
        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern UInt64 ph2_Detour_getFnAddress(IntPtr self);
        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern UInt64 ph2_Detour_getFnCallback(IntPtr self);
        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern UInt64* ph2_Detour_getUserTrampVar(IntPtr self);


        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static extern bool ph2_Detour_unHook(IntPtr self);

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static extern bool ph2_Detour_reHook(IntPtr self);

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern Mode ph2_Detour_getArchType(IntPtr self);

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern byte ph2_Detour_getMaxDepth(IntPtr self);

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern void ph2_Detour_setMaxDepth(IntPtr self, byte maxDepth);

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern HookType ph2_Detour_getType(IntPtr self);

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern void ph2_Detour_setIsFollowCallOnFnAddress(IntPtr self, [MarshalAs(UnmanagedType.I1)] bool value);


        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern void ph2_Detour_destructor(IntPtr self);

        //
        // IHook Interface
        //

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static extern bool ph2_IHook_hook(IntPtr self);

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static extern bool ph2_IHook_unHook(IntPtr self);

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static extern bool ph2_IHook_reHook(IntPtr self);

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static extern bool ph2_IHook_isHooked(IntPtr self);

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern HookType ph2_IHook_getType(IntPtr self);

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern void ph2_IHook_setDebug(IntPtr self, [MarshalAs(UnmanagedType.I1)] bool status);

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static extern bool ph2_IHook_setHooked(IntPtr self, [MarshalAs(UnmanagedType.I1)] bool state);

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern void ph2_IHook_destructor(IntPtr self);

        //
        // MemAccessor Interface
        //

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static extern bool ph2_MemAccessor_copy(IntPtr self, ulong dest, ulong src, ulong size);

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern ProtFlag ph2_MemAccessor_protect(IntPtr self, ulong dest, ulong size, ProtFlag newProtection, [MarshalAs(UnmanagedType.I1)] out bool status);

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static extern bool ph2_MemAccessor_safe_read(IntPtr self, ulong src, ulong dest, ulong size, out ulong read);

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static extern bool ph2_MemAccessor_safe_written(IntPtr self, ulong src, ulong dest, ulong size, out ulong written);

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl)]
        public static extern void ph2_MemAccessor_destructor(IntPtr self);
    }
}
