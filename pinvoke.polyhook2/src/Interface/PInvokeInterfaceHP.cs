﻿using PInvoke.PolyHook2.Enums;
using PInvoke.PolyHook2.Managed;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace PInvoke.PolyHook2
{
    /// <summary>
    /// PInvoke Interface for cPolyHook2
    /// (Function Pointer version)
    /// Info: All return bools need to be replaced by Bool32, as
    /// C++ defines them being one byte long, and since they get
    /// returned in the EAX register (x86), theres still some leftover
    /// trash in it, hence we use Bool32 to be able to parse the result.
    /// </summary>
    [SuppressUnmanagedCodeSecurity]
    public static unsafe class PInvokeInterfaceHP
    {
        public static void LoadFunctionPointers(delegate* managed<string, IntPtr> getProcAddressFn)
        {
#if NET8_0
            ph2_natDetour_create = (delegate* unmanaged[Cdecl, SuppressGCTransition]<UInt64, UInt64, UInt64*, IntPtr>)getProcAddressFn("ph2_natDetour_create");

            ph2_x86Detour_create = (delegate* unmanaged[Cdecl, SuppressGCTransition]<UInt64, UInt64, UInt64*, IntPtr>)getProcAddressFn("ph2_x86Detour_create");
            ph2_x86Detour_hook = (delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, Bool32>)getProcAddressFn("ph2_x86Detour_hook");
            ph2_x86Detour_getArchType = (delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, Mode>)getProcAddressFn("ph2_x86Detour_getArchType");
            ph2_x86Detour_destructor = (delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, void>)getProcAddressFn("ph2_x86Detour_destructor");

            ph2_x64Detour_create = (delegate* unmanaged[Cdecl, SuppressGCTransition]<UInt64, UInt64, UInt64*, IntPtr>)getProcAddressFn("ph2_x64Detour_create");
            ph2_x64Detour_hook = (delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, Bool32>)getProcAddressFn("ph2_x64Detour_hook");
            ph2_x64Detour_getArchType = (delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, Mode>)getProcAddressFn("ph2_x64Detour_getArchType");
            ph2_x64Detour_getDetourScheme = (delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, DetourScheme>)getProcAddressFn("ph2_x64Detour_getDetourScheme");
            ph2_x64Detour_setDetourScheme = (delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, DetourScheme, void>)getProcAddressFn("ph2_x64Detour_setDetourScheme");
            ph2_x64Detour_getMinJmpSize = (delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, byte>)getProcAddressFn("ph2_x64Detour_getMinJmpSize");
            ph2_x64Detour_printDetourScheme = (delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, DetourScheme, IntPtr>)getProcAddressFn("ph2_x64Detour_printDetourScheme");
            ph2_x64Detour_destructor = (delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, void>)getProcAddressFn("ph2_x64Detour_destructor");

            ph2_Detour_new = (delegate* unmanaged[Cdecl, SuppressGCTransition]<UInt64, UInt64, UInt64*, Mode, IntPtr>)getProcAddressFn("ph2_Detour_new");
            ph2_Detour_getFnAddress = (delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, UInt64>)getProcAddressFn("ph2_Detour_getFnAddress");
            ph2_Detour_getFnCallback = (delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, UInt64>)getProcAddressFn("ph2_Detour_getFnCallback");
            ph2_Detour_getUserTrampVar = (delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, UInt64*>)getProcAddressFn("ph2_Detour_getUserTrampVar");
            ph2_Detour_unHook = (delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, Bool32>)getProcAddressFn("ph2_Detour_unHook");
            ph2_Detour_reHook = (delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, Bool32>)getProcAddressFn("ph2_Detour_reHook");
            ph2_Detour_getArchType = (delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, Mode>)getProcAddressFn("ph2_Detour_getArchType");
            ph2_Detour_getMaxDepth = (delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, byte>)getProcAddressFn("ph2_Detour_getMaxDepth");
            ph2_Detour_setMaxDepth = (delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, byte, void>)getProcAddressFn("ph2_Detour_setMaxDepth");
            ph2_Detour_getType = (delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, HookType>)getProcAddressFn("ph2_Detour_getType");
            ph2_Detour_setIsFollowCallOnFnAddress = (delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, bool, void>)getProcAddressFn("ph2_Detour_setIsFollowCallOnFnAddress");
            ph2_Detour_destructor = (delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, void>)getProcAddressFn("ph2_Detour_destructor");

            ph2_IHook_hook = (delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, Bool32>)getProcAddressFn("ph2_IHook_hook");
            ph2_IHook_unHook = (delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, Bool32>)getProcAddressFn("ph2_IHook_unHook");
            ph2_IHook_reHook = (delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, Bool32>)getProcAddressFn("ph2_IHook_reHook");
            ph2_IHook_isHooked = (delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, Bool32>)getProcAddressFn("ph2_IHook_isHooked");
            ph2_IHook_getType = (delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, HookType>)getProcAddressFn("ph2_IHook_getType");
            ph2_IHook_setDebug = (delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, bool, void>)getProcAddressFn("ph2_IHook_setDebug");
            ph2_IHook_setHooked = (delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, bool, void>)getProcAddressFn("ph2_IHook_setHooked");
            ph2_IHook_destructor = (delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, void>)getProcAddressFn("ph2_IHook_destructor");

            ph2_MemAccessor_copy = (delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, UInt64, UInt64, UInt64, Bool32>)getProcAddressFn("ph2_MemAccessor_copy");
            ph2_MemAccessor_protect = (delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, UInt64, UInt64, ProtFlag, bool*, ProtFlag>)getProcAddressFn("ph2_MemAccessor_protect");
            ph2_MemAccessor_safe_read = (delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, UInt64, UInt64, UInt64, UInt64*, Bool32>)getProcAddressFn("ph2_MemAccessor_safe_read");
            ph2_MemAccessor_safe_write = (delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, UInt64, UInt64, UInt64, UInt64*, Bool32>)getProcAddressFn("ph2_MemAccessor_safe_write");
            ph2_MemAccessor_destructor = (delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, void>)getProcAddressFn("ph2_MemAccessor_destructor");
#else
            ph2_natDetour_create = (delegate* unmanaged[Cdecl]<UInt64, UInt64, UInt64*, IntPtr>)getProcAddressFn("ph2_natDetour_create");

            ph2_x86Detour_create = (delegate* unmanaged[Cdecl]<UInt64, UInt64, UInt64*, IntPtr>)getProcAddressFn("ph2_x86Detour_create");
            ph2_x86Detour_hook = (delegate* unmanaged[Cdecl]<IntPtr, Bool32>)getProcAddressFn("ph2_x86Detour_hook");
            ph2_x86Detour_getArchType = (delegate* unmanaged[Cdecl]<IntPtr, Mode>)getProcAddressFn("ph2_x86Detour_getArchType");
            ph2_x86Detour_destructor = (delegate* unmanaged[Cdecl]<IntPtr, void>)getProcAddressFn("ph2_x86Detour_destructor");

            ph2_x64Detour_create = (delegate* unmanaged[Cdecl]<UInt64, UInt64, UInt64*, IntPtr>)getProcAddressFn("ph2_x64Detour_create");
            ph2_x64Detour_hook = (delegate* unmanaged[Cdecl]<IntPtr, Bool32>)getProcAddressFn("ph2_x64Detour_hook");
            ph2_x64Detour_getArchType = (delegate* unmanaged[Cdecl]<IntPtr, Mode>)getProcAddressFn("ph2_x64Detour_getArchType");
            ph2_x64Detour_getDetourScheme = (delegate* unmanaged[Cdecl]<IntPtr, DetourScheme>)getProcAddressFn("ph2_x64Detour_getDetourScheme");
            ph2_x64Detour_setDetourScheme = (delegate* unmanaged[Cdecl]<IntPtr, DetourScheme, void>)getProcAddressFn("ph2_x64Detour_setDetourScheme");
            ph2_x64Detour_getMinJmpSize = (delegate* unmanaged[Cdecl]<IntPtr, byte>)getProcAddressFn("ph2_x64Detour_getMinJmpSize");
            ph2_x64Detour_printDetourScheme = (delegate* unmanaged[Cdecl]<IntPtr, DetourScheme, IntPtr>)getProcAddressFn("ph2_x64Detour_printDetourScheme");
            ph2_x64Detour_destructor = (delegate* unmanaged[Cdecl]<IntPtr, void>)getProcAddressFn("ph2_x64Detour_destructor");

            ph2_Detour_new = (delegate* unmanaged[Cdecl]<UInt64, UInt64, UInt64*, Mode, IntPtr>)getProcAddressFn("ph2_Detour_new");
            ph2_Detour_getFnAddress = (delegate* unmanaged[Cdecl]<IntPtr, UInt64>)getProcAddressFn("ph2_Detour_getFnAddress");
            ph2_Detour_getFnCallback = (delegate* unmanaged[Cdecl]<IntPtr, UInt64>)getProcAddressFn("ph2_Detour_getFnCallback");
            ph2_Detour_getUserTrampVar = (delegate* unmanaged[Cdecl]<IntPtr, UInt64*>)getProcAddressFn("ph2_Detour_getUserTrampVar");
            ph2_Detour_unHook = (delegate* unmanaged[Cdecl]<IntPtr, Bool32>)getProcAddressFn("ph2_Detour_unHook");
            ph2_Detour_reHook = (delegate* unmanaged[Cdecl]<IntPtr, Bool32>)getProcAddressFn("ph2_Detour_reHook");
            ph2_Detour_getArchType = (delegate* unmanaged[Cdecl]<IntPtr, Mode>)getProcAddressFn("ph2_Detour_getArchType");
            ph2_Detour_getMaxDepth = (delegate* unmanaged[Cdecl]<IntPtr, byte>)getProcAddressFn("ph2_Detour_getMaxDepth");
            ph2_Detour_setMaxDepth = (delegate* unmanaged[Cdecl]<IntPtr, byte, void>)getProcAddressFn("ph2_Detour_setMaxDepth");
            ph2_Detour_getType = (delegate* unmanaged[Cdecl]<IntPtr, HookType>)getProcAddressFn("ph2_Detour_getType");
            ph2_Detour_setIsFollowCallOnFnAddress = (delegate* unmanaged[Cdecl]<IntPtr, bool, void>)getProcAddressFn("ph2_Detour_setIsFollowCallOnFnAddress");
            ph2_Detour_destructor = (delegate* unmanaged[Cdecl]<IntPtr, void>)getProcAddressFn("ph2_Detour_destructor");

            ph2_IHook_hook = (delegate* unmanaged[Cdecl]<IntPtr, Bool32>)getProcAddressFn("ph2_IHook_hook");
            ph2_IHook_unHook = (delegate* unmanaged[Cdecl]<IntPtr, Bool32>)getProcAddressFn("ph2_IHook_unHook");
            ph2_IHook_reHook = (delegate* unmanaged[Cdecl]<IntPtr, Bool32>)getProcAddressFn("ph2_IHook_reHook");
            ph2_IHook_isHooked = (delegate* unmanaged[Cdecl]<IntPtr, Bool32>)getProcAddressFn("ph2_IHook_isHooked");
            ph2_IHook_getType = (delegate* unmanaged[Cdecl]<IntPtr, HookType>)getProcAddressFn("ph2_IHook_getType");
            ph2_IHook_setDebug = (delegate* unmanaged[Cdecl]<IntPtr, bool, void>)getProcAddressFn("ph2_IHook_setDebug");
            ph2_IHook_setHooked = (delegate* unmanaged[Cdecl]<IntPtr, bool, void>)getProcAddressFn("ph2_IHook_setHooked");
            ph2_IHook_destructor = (delegate* unmanaged[Cdecl]<IntPtr, void>)getProcAddressFn("ph2_IHook_destructor");

            ph2_MemAccessor_copy = (delegate* unmanaged[Cdecl]<IntPtr, UInt64, UInt64, UInt64, Bool32>)getProcAddressFn("ph2_MemAccessor_copy");
            ph2_MemAccessor_protect = (delegate* unmanaged[Cdecl]<IntPtr, UInt64, UInt64, ProtFlag, bool*, ProtFlag>)getProcAddressFn("ph2_MemAccessor_protect");
            ph2_MemAccessor_safe_read = (delegate* unmanaged[Cdecl]<IntPtr, UInt64, UInt64, UInt64, UInt64*, Bool32>)getProcAddressFn("ph2_MemAccessor_safe_read");
            ph2_MemAccessor_safe_write = (delegate* unmanaged[Cdecl]<IntPtr, UInt64, UInt64, UInt64, UInt64*, Bool32>)getProcAddressFn("ph2_MemAccessor_safe_write");
            ph2_MemAccessor_destructor = (delegate* unmanaged[Cdecl]<IntPtr, void>)getProcAddressFn("ph2_MemAccessor_destructor");
#endif
        }

#if NET8_0
        //
        // natDetour
        //
        public static delegate* unmanaged[Cdecl, SuppressGCTransition]<UInt64, UInt64, UInt64*, IntPtr> ph2_natDetour_create;

        //
        // X86Detour
        //
        public static delegate* unmanaged[Cdecl, SuppressGCTransition]<UInt64, UInt64, UInt64*, IntPtr> ph2_x86Detour_create;
        public static delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, Bool32> ph2_x86Detour_hook;
        public static delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, Mode> ph2_x86Detour_getArchType;
        public static delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, void> ph2_x86Detour_destructor;

        //
        // x64Detour
        //
        public static delegate* unmanaged[Cdecl, SuppressGCTransition]<UInt64, UInt64, UInt64*, IntPtr> ph2_x64Detour_create;
        public static delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, Bool32> ph2_x64Detour_hook;
        public static delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, Mode> ph2_x64Detour_getArchType;
        public static delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, DetourScheme> ph2_x64Detour_getDetourScheme;
        public static delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, DetourScheme, void> ph2_x64Detour_setDetourScheme;
        public static delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, byte> ph2_x64Detour_getMinJmpSize;
        public static delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, DetourScheme, IntPtr> ph2_x64Detour_printDetourScheme;
        public static delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, void> ph2_x64Detour_destructor;

        //
        // ADetour
        //
        public static delegate* unmanaged[Cdecl, SuppressGCTransition]<UInt64, UInt64, UInt64*, Mode, IntPtr> ph2_Detour_new;
        public static delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, UInt64> ph2_Detour_getFnAddress;
        public static delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, UInt64> ph2_Detour_getFnCallback;
        public static delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, UInt64*> ph2_Detour_getUserTrampVar;

        public static delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, Bool32> ph2_Detour_unHook;
        public static delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, Bool32> ph2_Detour_reHook;
        public static delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, Mode> ph2_Detour_getArchType;
        public static delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, byte> ph2_Detour_getMaxDepth;
        public static delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, byte, void> ph2_Detour_setMaxDepth;
        public static delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, HookType> ph2_Detour_getType;
        public static delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, bool, void> ph2_Detour_setIsFollowCallOnFnAddress;
        public static delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, void> ph2_Detour_destructor;

        //
        // IHook
        //
        public static delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, Bool32> ph2_IHook_hook;
        public static delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, Bool32> ph2_IHook_unHook;
        public static delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, Bool32> ph2_IHook_reHook;
        public static delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, Bool32> ph2_IHook_isHooked;
        public static delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, HookType> ph2_IHook_getType;
        public static delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, bool, void> ph2_IHook_setDebug;
        public static delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, bool, void> ph2_IHook_setHooked;
        public static delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, void> ph2_IHook_destructor;

        //
        // MemAccessor
        //
        public static delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, UInt64, UInt64, UInt64, Bool32> ph2_MemAccessor_copy;
        public static delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, UInt64, UInt64, ProtFlag, bool*, ProtFlag> ph2_MemAccessor_protect;
        public static delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, UInt64, UInt64, UInt64, UInt64*, Bool32> ph2_MemAccessor_safe_read;
        public static delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, UInt64, UInt64, UInt64, UInt64*, Bool32> ph2_MemAccessor_safe_write;
        public static delegate* unmanaged[Cdecl, SuppressGCTransition]<IntPtr, void> ph2_MemAccessor_destructor;
#else
        //
        // natDetour
        //
        public static delegate* unmanaged[Cdecl]<UInt64, UInt64, UInt64*, IntPtr> ph2_natDetour_create;

        //
        // X86Detour
        //
        public static delegate* unmanaged[Cdecl]<UInt64, UInt64, UInt64*, IntPtr> ph2_x86Detour_create;
        public static delegate* unmanaged[Cdecl]<IntPtr, Bool32> ph2_x86Detour_hook;
        public static delegate* unmanaged[Cdecl]<IntPtr, Mode> ph2_x86Detour_getArchType;
        public static delegate* unmanaged[Cdecl]<IntPtr, void> ph2_x86Detour_destructor;

        //
        // x64Detour
        //
        public static delegate* unmanaged[Cdecl]<UInt64, UInt64, UInt64*, IntPtr> ph2_x64Detour_create;
        public static delegate* unmanaged[Cdecl]<IntPtr, Bool32> ph2_x64Detour_hook;
        public static delegate* unmanaged[Cdecl]<IntPtr, Mode> ph2_x64Detour_getArchType;
        public static delegate* unmanaged[Cdecl]<IntPtr, DetourScheme> ph2_x64Detour_getDetourScheme;
        public static delegate* unmanaged[Cdecl]<IntPtr, DetourScheme, void> ph2_x64Detour_setDetourScheme;
        public static delegate* unmanaged[Cdecl]<IntPtr, byte> ph2_x64Detour_getMinJmpSize;
        public static delegate* unmanaged[Cdecl]<IntPtr, DetourScheme, IntPtr> ph2_x64Detour_printDetourScheme;
        public static delegate* unmanaged[Cdecl]<IntPtr, void> ph2_x64Detour_destructor;

        //
        // ADetour
        //
        public static delegate* unmanaged[Cdecl]<UInt64, UInt64, UInt64*, Mode, IntPtr> ph2_Detour_new;
        public static delegate* unmanaged[Cdecl]<IntPtr, UInt64> ph2_Detour_getFnAddress;
        public static delegate* unmanaged[Cdecl]<IntPtr, UInt64> ph2_Detour_getFnCallback;
        public static delegate* unmanaged[Cdecl]<IntPtr, UInt64*> ph2_Detour_getUserTrampVar;

        public static delegate* unmanaged[Cdecl]<IntPtr, Bool32> ph2_Detour_unHook;
        public static delegate* unmanaged[Cdecl]<IntPtr, Bool32> ph2_Detour_reHook;
        public static delegate* unmanaged[Cdecl]<IntPtr, Mode> ph2_Detour_getArchType;
        public static delegate* unmanaged[Cdecl]<IntPtr, byte> ph2_Detour_getMaxDepth;
        public static delegate* unmanaged[Cdecl]<IntPtr, byte, void> ph2_Detour_setMaxDepth;
        public static delegate* unmanaged[Cdecl]<IntPtr, HookType> ph2_Detour_getType;
        public static delegate* unmanaged[Cdecl]<IntPtr, bool, void> ph2_Detour_setIsFollowCallOnFnAddress;
        public static delegate* unmanaged[Cdecl]<IntPtr, void> ph2_Detour_destructor;

        //
        // IHook
        //
        public static delegate* unmanaged[Cdecl]<IntPtr, Bool32> ph2_IHook_hook;
        public static delegate* unmanaged[Cdecl]<IntPtr, Bool32> ph2_IHook_unHook;
        public static delegate* unmanaged[Cdecl]<IntPtr, Bool32> ph2_IHook_reHook;
        public static delegate* unmanaged[Cdecl]<IntPtr, Bool32> ph2_IHook_isHooked;
        public static delegate* unmanaged[Cdecl]<IntPtr, HookType> ph2_IHook_getType;
        public static delegate* unmanaged[Cdecl]<IntPtr, bool, void> ph2_IHook_setDebug;
        public static delegate* unmanaged[Cdecl]<IntPtr, bool, void> ph2_IHook_setHooked;
        public static delegate* unmanaged[Cdecl]<IntPtr, void> ph2_IHook_destructor;

        //
        // MemAccessor
        //
        public static delegate* unmanaged[Cdecl]<IntPtr, UInt64, UInt64, UInt64, Bool32> ph2_MemAccessor_copy;
        public static delegate* unmanaged[Cdecl]<IntPtr, UInt64, UInt64, ProtFlag, bool*, ProtFlag> ph2_MemAccessor_protect;
        public static delegate* unmanaged[Cdecl]<IntPtr, UInt64, UInt64, UInt64, UInt64*, Bool32> ph2_MemAccessor_safe_read;
        public static delegate* unmanaged[Cdecl]<IntPtr, UInt64, UInt64, UInt64, UInt64*, Bool32> ph2_MemAccessor_safe_write;
        public static delegate* unmanaged[Cdecl]<IntPtr, void> ph2_MemAccessor_destructor;
#endif
    }
}
