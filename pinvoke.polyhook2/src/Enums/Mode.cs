﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PInvoke.PolyHook2.Enums
{
    public enum Mode : uint
    {
        x86 = 0,
        x64 = 1
    }
}
