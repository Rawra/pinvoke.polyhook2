﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PInvoke.PolyHook2.Enums
{
    public enum ProtFlag : byte
    {
        UNSET = 0,   // Value means this give no information about protection state (un-read)
        X = 1 << 1,
        R = 1 << 2,
        W = 1 << 3,
        S = 1 << 4,
        P = 1 << 5,
        NONE = 1 << 6,   // The value equaling the linux flag PROT_UNSET (read the prot, and the prot is unset)
        RWX = R | W | X
    }
}
