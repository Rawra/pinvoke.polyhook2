﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PInvoke.PolyHook2.Enums
{
    public enum DetourScheme : byte
    {
        VALLOC2 = 1 << 0,       // use virtualalloc2 to allocate in range. Only on win10 > 1803
        INPLACE = 1 << 1,       // use push-ret for fnCallback in-place storage.
        CODE_CAVE = 1 << 2,     // searching for code-cave to keep fnCallback.
        INPLACE_SHORT = 1 << 3, // spoils rax register
        RECOMMENDED = VALLOC2 | INPLACE | CODE_CAVE,
        // first try to allocate, then fallback to code cave if not supported.
        // will not fallback on failure of allocation
        ALL = RECOMMENDED | INPLACE_SHORT
    }
}
