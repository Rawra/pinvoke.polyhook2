﻿using PInvoke.PolyHook2.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PInvoke.PolyHook2
{
    public class IHook : MemAccessor
    {
        public IHook(IntPtr handle) : base(handle) { }

        protected override bool ReleaseHandle()
        {
            PInvokeInterface.ph2_IHook_destructor(handle);
            return true;
        }

        public virtual bool Hook()
        {
            EnsureHandle();
            return PInvokeInterface.ph2_IHook_hook(handle);
        }

        public virtual bool UnHook()
        {
            EnsureHandle();
            return PInvokeInterface.ph2_IHook_unHook(handle);
        }

        public virtual bool ReHook()
        {
            EnsureHandle();
            return PInvokeInterface.ph2_IHook_reHook(handle);
        }

        public virtual bool SetHooked(bool state)
        {
            EnsureHandle();
            return PInvokeInterface.ph2_IHook_setHooked(handle, state);
        }

        public virtual bool IsHooked()
        {
            EnsureHandle();
            return PInvokeInterface.ph2_IHook_isHooked(handle);
        }

        public virtual HookType GetHookType()
        {
            EnsureHandle();
            return PInvokeInterface.ph2_IHook_getType(handle);
        }

        public virtual void SetDebug(bool state)
        {
            EnsureHandle();
            PInvokeInterface.ph2_IHook_setDebug(handle, state);
        }
        
    }
}
