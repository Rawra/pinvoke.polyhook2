﻿using PInvoke.PolyHook2.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PInvoke.PolyHook2
{
    public class MemAccessor : SafeHandle
    {
        public MemAccessor() : base(IntPtr.Zero, ownsHandle: true) { }
        public MemAccessor(IntPtr handle) : base(handle, ownsHandle: true) { }
        public override bool IsInvalid => handle == IntPtr.Zero;

        protected override bool ReleaseHandle()
        {
            PInvokeInterface.ph2_MemAccessor_destructor(handle);
            return true;
        }

        public virtual bool MemCopy(UInt64 dest, UInt64 src, UInt64 size)
        {
            EnsureHandle();
            return PInvokeInterface.ph2_MemAccessor_copy(handle, dest, src, size);
        }

        public virtual bool SafeMemWrite(UInt64 dest, UInt64 src, UInt64 size, out UInt64 written)
        {
            EnsureHandle();
            return PInvokeInterface.ph2_MemAccessor_safe_written(handle, dest, src, size, out written);
        }

        public virtual bool SafeMemRead(UInt64 dest, UInt64 src, UInt64 size, out UInt64 read)
        {
            EnsureHandle();
            return PInvokeInterface.ph2_MemAccessor_safe_read(handle, dest, src, size, out read);
        }

        public virtual ProtFlag MemProtect(UInt64 dest, UInt64 size, ProtFlag newProtection, out bool status)
        {
            EnsureHandle();
            return PInvokeInterface.ph2_MemAccessor_protect(handle, dest, size, newProtection, out status);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected void EnsureHandle()
        {
            if (IsInvalid)
                throw new InvalidOperationException("handle is not initialized or has already been disposed.");
        }
    }
}
