﻿using PInvoke.PolyHook2.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PInvoke.PolyHook2
{
    public class ADetour : IHook
    {
        public ADetour(IntPtr handle) : base(handle) { }

        public UInt64 fnAddress;
        public UInt64 fnCallbackAddress;
        public UInt64 userTrampVar;

        protected override bool ReleaseHandle()
        {
            PInvokeInterface.ph2_Detour_destructor(handle);
            return true;
        }

        public override bool UnHook()
        {
            EnsureHandle();
            return PInvokeInterface.ph2_Detour_unHook(handle);
        }

        /// <summary>
        /// This is for restoring hook bytes if a 3rd party uninstalled them.
        /// DO NOT call this after unHook(). This may only be called after hook()
        /// but before unHook()
        /// </summary>
        /// <returns></returns>
        public override bool ReHook()
        {
            return PInvokeInterface.ph2_Detour_reHook(handle);
        }

        public virtual Mode GetArchType()
        {
            return PInvokeInterface.ph2_Detour_getArchType(handle);
        }

        public virtual byte GetMaxDepth()
        {
            return PInvokeInterface.ph2_Detour_getMaxDepth(handle);
        }

        public virtual void SetMaxDepth(byte maxDepth)
        {
            PInvokeInterface.ph2_Detour_setMaxDepth(handle, maxDepth);
        }

        public override HookType GetHookType()
        {
            return PInvokeInterface.ph2_Detour_getType(handle);
        }

        public virtual void SetIsFollowCallOnFnAddress(bool value)
        {
            PInvokeInterface.ph2_Detour_setIsFollowCallOnFnAddress(handle, value);
        }
        
    }
}
