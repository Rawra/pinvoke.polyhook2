﻿using PInvoke.PolyHook2.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PInvoke.PolyHook2
{
    public class X86Detour : ADetour
    {
        public X86Detour(IntPtr handle) : base(handle) { }
        public X86Detour(UInt64 fnAddress, UInt64 fnCallback, out UInt64 userTrampVar) : base (IntPtr.Zero)
        {
            handle = PInvokeInterface.ph2_x86Detour_create(fnAddress, fnCallback, out userTrampVar);
        }

        public static X86Detour Create(UInt64 fnAddress, UInt64 fnCallback, out UInt64 userTrampVar)
        {
            return new X86Detour(PInvokeInterface.ph2_x86Detour_create(fnAddress, fnCallback, out userTrampVar));
        }

        protected override bool ReleaseHandle()
        {
            PInvokeInterface.ph2_x86Detour_destructor(handle);
            return true;
        }

        public override bool Hook()
        {
            EnsureHandle();
            return PInvokeInterface.ph2_x86Detour_hook(handle);
        }
        public override Mode GetArchType()
        {
            EnsureHandle();
            return PInvokeInterface.ph2_x86Detour_getArchType(handle);
        }
    }
}
