﻿using PInvoke.PolyHook2.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PInvoke.PolyHook2
{
    public class X64Detour : ADetour
    {
        public X64Detour(IntPtr handle) : base(handle) { }
        public X64Detour(UInt64 fnAddress, UInt64 fnCallback, out UInt64 userTrampVar) : base (IntPtr.Zero)
        {
            handle = PInvokeInterface.ph2_x64Detour_create(fnAddress, fnCallback, out userTrampVar);
        }

        public static X64Detour Create(UInt64 fnAddress, UInt64 fnCallback, out UInt64 userTrampVar)
        {
            return new X64Detour(PInvokeInterface.ph2_x64Detour_create(fnAddress, fnCallback, out userTrampVar));
        }

        protected override bool ReleaseHandle()
        {
            PInvokeInterface.ph2_x64Detour_destructor(handle);
            return true;
        }

        public override bool Hook()
        {
            EnsureHandle();
            return PInvokeInterface.ph2_x64Detour_hook(handle);
        }
        public override Mode GetArchType()
        {
            EnsureHandle();
            return PInvokeInterface.ph2_x64Detour_getArchType(handle);
        }
        public virtual DetourScheme GetDetourScheme()
        {
            EnsureHandle();
            return PInvokeInterface.ph2_x64Detour_getDetourScheme(handle);
        }
        public virtual void SetDetourScheme(DetourScheme scheme)
        {
            EnsureHandle();
            PInvokeInterface.ph2_x64Detour_setDetourScheme(handle, scheme);
        }
        public static byte GetMinJmpSize()
        {
            return PInvokeInterface.ph2_x64Detour_getMinJmpSize();
        }
        public static string PrintDetourScheme(DetourScheme scheme)
        {
            return PInvokeInterface.ph2_x64Detour_printDetourScheme(scheme);
        }

    }
}
