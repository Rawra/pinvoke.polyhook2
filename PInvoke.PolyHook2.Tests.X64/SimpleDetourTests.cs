﻿
using PInvoke.PolyHook2.Managed;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
namespace PInvoke.PolyHook2.Tests;

/// <summary>
/// Coverage:
/// - Apply single hook
/// - Uninstall single hook and dispose
/// 
/// </summary>
public unsafe class SimpleDetourTests
{
    public SimpleDetourTests()
    {
        string currentDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)!;
        string depsDirectory = Path.GetFullPath(Path.Combine(currentDirectory, "..", "..", "..", "..", "deps", "x86_64"));

        // Add deps to PATH
        if (!Directory.Exists(depsDirectory))
            throw new DirectoryNotFoundException($"Dependency directory not found: {depsDirectory}");

        string? currentPath = System.Environment.GetEnvironmentVariable("PATH");
        if (currentPath == null)
            throw new Exception("Could not retrieve system PATH");

        if (!currentPath.Contains(depsDirectory))
        {
            System.Environment.SetEnvironmentVariable("PATH", currentPath + $";{depsDirectory}");
        }
        Trace.WriteLine($"PATH={Environment.GetEnvironmentVariable("PATH")}");

        // Load function pointers (HP version)
        PInvokeInterfaceHP.LoadFunctionPointers(&GetProcAddressProxy);
    }


    ~SimpleDetourTests()
    {
        if (_cpolyhook2Handle != IntPtr.Zero)
        {
            FreeLibrary(_cpolyhook2Handle);
        }
    }

    [DllImport("kernel32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Winapi)]
    public extern static IntPtr LoadLibraryA(string lpLibFileName);
    [DllImport("kernel32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Winapi, SetLastError = true)]
    public static extern bool FreeLibrary(nint hLibModule);
    [DllImport("kernel32.dll", CharSet = CharSet.Ansi, ExactSpelling = true, SetLastError = true)]
    public static extern IntPtr GetProcAddress(IntPtr hModule, string procName);
    public static IntPtr _cpolyhook2Handle = IntPtr.Zero;

    private static IntPtr GetProcAddressProxy(string lpProcName)
    {
        if (_cpolyhook2Handle == IntPtr.Zero)
        {
            _cpolyhook2Handle = LoadLibraryA("cpolyhook2.dll");
            if (_cpolyhook2Handle == IntPtr.Zero)
                throw new DllNotFoundException("could not load cpolyhook2.dll");
        }
        return GetProcAddress(_cpolyhook2Handle, lpProcName);
    }

    //
    // Test: Apply single hook and Unhook (Function Pointer, Marshalled)
    //

    [UnmanagedFunctionPointer(CallingConvention.Winapi)]
    [return: MarshalAs(UnmanagedType.Bool)]
    private delegate bool deletefilea_delegate([MarshalAs(UnmanagedType.LPStr)] string lpFileName);

    [Fact]
    public unsafe void TestHookDeleteFileA_FunctionPointer()
    {
        UInt64 fnAddress = (UInt64)GetProcAddress(LoadLibraryA("kernel32.dll"), "DeleteFileA");
        delegate* unmanaged[Stdcall]<IntPtr, bool> call = (delegate* unmanaged[Stdcall]<IntPtr, bool>)fnAddress;

        // Make sure it works without hooking first
        Assert.False(call(IntPtr.Zero));
        X64SimpleDetour hook = new X64SimpleDetour(fnAddress, (UInt64)(delegate* unmanaged[Stdcall]<IntPtr, int>)&TestHookImplFunctionPointerDeleteFileA, doAutoHook: false);

        // Make sure the hook is not yet applied and a detour handle exists
        Assert.False(call(IntPtr.Zero));

        // Make sure is not hooked
        Assert.False(hook.IsHooked());

        // Make sure the hook does what its supposed to when applied properly.
        Assert.True(hook.Hook());

        // Make sure the trampoline works aswell
        Assert.Equal(0, ((delegate* unmanaged[Stdcall]<IntPtr, int>)hook.TrampolineAddress)(IntPtr.Zero));

        // Re-Hook
        Assert.True(hook.ReHook());
        Assert.True(call(IntPtr.Zero));

        // Make sure the clean up process works aswell
        Assert.True(hook.UnHook());
        Assert.False(call(IntPtr.Zero));
    }

    [UnmanagedCallersOnly(CallConvs = [typeof(CallConvStdcall)])]
    public static int TestHookImplFunctionPointerDeleteFileA(IntPtr lpFileName)
    {
        return 1;
    }

    [Fact]
    public unsafe void TestHookDeleteFileA_Marshalled()
    {
        UInt64 fnAddress = (UInt64)GetProcAddress(LoadLibraryA("kernel32.dll"), "DeleteFileA");
        deletefilea_delegate call = Marshal.GetDelegateForFunctionPointer<deletefilea_delegate>((IntPtr)fnAddress);

        // Make sure it works without hooking first
        Assert.False(call("_SHOULD_NOT_EXIST_"));
        X64SimpleDetour hook = new X64SimpleDetour(fnAddress, (UInt64)Marshal.GetFunctionPointerForDelegate<deletefilea_delegate>(TestHookImplMarshalledDeleteFileA), doAutoHook: false);

        // Make sure the hook is not yet applied and a detour handle exists
        Assert.False(call("_SHOULD_NOT_EXIST_"));

        // Make sure is not hooked
        Assert.False(hook.IsHooked());

        // Make sure the hook does what its supposed to when applied properly.
        Assert.True(hook.Hook());

        // Make sure the trampoline works aswell
        Assert.False(Marshal.GetDelegateForFunctionPointer<deletefilea_delegate>((IntPtr)hook.TrampolineAddress)("_SHOULD_NOT_EXIST_"));

        // Re-Hook
        Assert.True(hook.ReHook());
        Assert.True(call("_SHOULD_NOT_EXIST_"));

        // Make sure the clean up process works aswell
        Assert.True(hook.UnHook());
        Assert.False(call("_SHOULD_NOT_EXIST_"));
    }

    public static bool TestHookImplMarshalledDeleteFileA(string lpFileName)
    {
        return true;
    }
}