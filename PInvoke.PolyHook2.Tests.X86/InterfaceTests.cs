﻿
using PInvoke.PolyHook2.Managed;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
namespace PInvoke.PolyHook2.Tests;

/// <summary>
/// Coverage:
/// - Apply single hook
/// - Uninstall single hook and dispose
/// </summary>
public unsafe class InterfaceTests
{
    [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
    public static extern int MessageBoxA(IntPtr hWnd, [MarshalAs(UnmanagedType.LPStr)] string text, [MarshalAs(UnmanagedType.LPStr)] string caption, uint type);

    public InterfaceTests()
    {
        string currentDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)!;
        string depsDirectory = Path.GetFullPath(Path.Combine(currentDirectory, "..", "..", "..", "..", "deps", "x86"));

        // Add deps to PATH
        if (!Directory.Exists(depsDirectory))
            throw new DirectoryNotFoundException($"Dependency directory not found: {depsDirectory}");

        string? currentPath = System.Environment.GetEnvironmentVariable("PATH");
        if (currentPath == null)
            throw new Exception("Could not retrieve system PATH");

        if (!currentPath.Contains(depsDirectory))
        {
            System.Environment.SetEnvironmentVariable("PATH", currentPath + $";{depsDirectory}");
        }
        Trace.WriteLine($"PATH={Environment.GetEnvironmentVariable("PATH")}");

        // Load function pointers (HP version)
        PInvokeInterfaceHP.LoadFunctionPointers(&GetProcAddressProxy);
    }

   
    ~InterfaceTests()
    {
        if (_cpolyhook2Handle != IntPtr.Zero)
        {
            FreeLibrary(_cpolyhook2Handle);
        }
    }

    [DllImport("kernel32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Winapi)]
    public extern static IntPtr LoadLibraryA(string lpLibFileName);
    [DllImport("kernel32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Winapi, SetLastError = true)]
    public static extern bool FreeLibrary(nint hLibModule);
    [DllImport("kernel32.dll", CharSet = CharSet.Ansi, ExactSpelling = true, SetLastError = true)]
    public static extern IntPtr GetProcAddress(IntPtr hModule, string procName);
    public static IntPtr _cpolyhook2Handle = IntPtr.Zero;

    private static IntPtr GetProcAddressProxy(string lpProcName)
    {
        if (_cpolyhook2Handle == IntPtr.Zero)
        {
            _cpolyhook2Handle = LoadLibraryA("cpolyhook2.dll");
            if (_cpolyhook2Handle == IntPtr.Zero)
                throw new DllNotFoundException("could not load cpolyhook2.dll");
        }
        return GetProcAddress(_cpolyhook2Handle, lpProcName);
    }

    //
    // Test: Apply single hook and Unhook (Function Pointer, Marshalled)
    //

    [Fact]
    public unsafe void TestHookDeleteFileA_FunctionPointer()
    {
        UInt64 fnAddress = (UInt64)GetProcAddress(LoadLibraryA("kernel32.dll"), "DeleteFileA");
        delegate* unmanaged[Stdcall]<IntPtr, bool> call = (delegate* unmanaged[Stdcall]<IntPtr, bool>)fnAddress;

        // Make sure it works without hooking first
        Assert.False(call(IntPtr.Zero));
        IntPtr handle = PInvokeInterface.ph2_x86Detour_create(fnAddress, (UInt64)(delegate* unmanaged[Stdcall]<IntPtr, int>)&TestHookImplFunctionPointerDeleteFileA, out UInt64 userTrampVar);

        // Make sure the hook is not yet applied and a detour handle exists
        Assert.False(call(IntPtr.Zero));
        Assert.NotEqual(IntPtr.Zero, handle);

        // Make sure is not hooked
        Assert.False(PInvokeInterface.ph2_IHook_isHooked(handle));
        Assert.NotEqual(IntPtr.Zero, handle);

        // Make sure the hook does what its supposed to when applied properly.
        Assert.True(PInvokeInterface.ph2_x86Detour_hook(handle));
        Assert.NotEqual(IntPtr.Zero, handle);

        // Make sure the trampoline works aswell
        Assert.Equal(0, ((delegate* unmanaged[Stdcall]<IntPtr, int>)userTrampVar)(IntPtr.Zero));
        Assert.NotEqual(IntPtr.Zero, handle);

        // Re-Hook
        Assert.True(PInvokeInterface.ph2_Detour_reHook(handle));
        Assert.True(call(IntPtr.Zero));

        // Make sure the clean up process works aswell
        Assert.True(PInvokeInterface.ph2_Detour_unHook(handle));
        Assert.False(call(IntPtr.Zero));
    }
 
    [Fact]
    public unsafe void TestHookDeleteFileA_FunctionPointerHP()
    {
        UInt64 fnAddress = (UInt64)GetProcAddress(LoadLibraryA("kernel32.dll"), "DeleteFileA");
        delegate* unmanaged[Stdcall]<IntPtr, bool> call = (delegate* unmanaged[Stdcall]<IntPtr, bool>)fnAddress;

        // Make sure it works without hooking first
        Assert.False(call(IntPtr.Zero));
        UInt64 userTrampVar = 0;
        IntPtr handle = PInvokeInterfaceHP.ph2_x86Detour_create(fnAddress, (UInt64)(delegate* unmanaged[Stdcall]<IntPtr, int>)&TestHookImplFunctionPointerDeleteFileA, &userTrampVar);

        // Make sure the hook is not yet applied and a detour handle exists
        Assert.False(call(IntPtr.Zero));
        Assert.NotEqual(IntPtr.Zero, handle);

        // Make sure is not hooked
        Assert.True(!PInvokeInterfaceHP.ph2_IHook_isHooked(handle).IsTrue());
        Assert.NotEqual(IntPtr.Zero, handle);

        // Make sure the hook does what its supposed to when applied properly.
        Assert.True(PInvokeInterfaceHP.ph2_x86Detour_hook(handle).IsTrue());
        Assert.NotEqual(IntPtr.Zero, handle);

        // Make sure the trampoline works aswell
        Assert.Equal(0, ((delegate* unmanaged[Stdcall]<IntPtr, int>)userTrampVar)(IntPtr.Zero));
        Assert.NotEqual(IntPtr.Zero, handle);

        // Re-Hook
        Assert.True(PInvokeInterfaceHP.ph2_Detour_reHook(handle).IsTrue());
        Assert.True(call(IntPtr.Zero));

        // Make sure the clean up process works aswell
        Assert.True(PInvokeInterfaceHP.ph2_Detour_unHook(handle).IsTrue());
        Assert.False(call(IntPtr.Zero));
    }

    [UnmanagedCallersOnly(CallConvs = [typeof(CallConvStdcall)])]
    public static int TestHookImplFunctionPointerDeleteFileA(IntPtr lpFileName)
    {
        return 1;
    }

    [UnmanagedFunctionPointer(CallingConvention.Winapi)]
    [return: MarshalAs(UnmanagedType.Bool)]
    private delegate bool deletefilea_delegate([MarshalAs(UnmanagedType.LPStr)] string lpFileName);

    [Fact]
    public unsafe void TestHookDeleteFileA_Marshalled()
    {
        UInt64 fnAddress = (UInt64)GetProcAddress(LoadLibraryA("kernel32.dll"), "DeleteFileA");
        deletefilea_delegate call = Marshal.GetDelegateForFunctionPointer<deletefilea_delegate>((IntPtr)fnAddress);

        // Make sure it works without hooking first
        Assert.False(call("_SHOULD_NOT_EXIST_"));
        IntPtr handle = PInvokeInterface.ph2_x86Detour_create(fnAddress, (UInt64)Marshal.GetFunctionPointerForDelegate<deletefilea_delegate>(TestHookImplMarshalledDeleteFileA), out UInt64 userTrampVar);

        // Make sure the hook is not yet applied and a detour handle exists
        Assert.False(call("_SHOULD_NOT_EXIST_"));
        Assert.NotEqual(IntPtr.Zero, handle);

        // Make sure is not hooked
        Assert.False(PInvokeInterface.ph2_IHook_isHooked(handle));
        Assert.NotEqual(IntPtr.Zero, handle);

        // Make sure the hook does what its supposed to when applied properly.
        Assert.True(PInvokeInterface.ph2_x86Detour_hook(handle));
        Assert.NotEqual(IntPtr.Zero, handle);

        // Make sure the trampoline works aswell
        Assert.False(Marshal.GetDelegateForFunctionPointer<deletefilea_delegate>((IntPtr)userTrampVar)("_SHOULD_NOT_EXIST_"));
        Assert.NotEqual(IntPtr.Zero, handle);

        // Re-Hook
        Assert.True(PInvokeInterface.ph2_Detour_reHook(handle));
        Assert.True(call("_SHOULD_NOT_EXIST_"));

        // Make sure the clean up process works aswell
        Assert.True(PInvokeInterface.ph2_Detour_unHook(handle));
        Assert.False(call("_SHOULD_NOT_EXIST_"));
    }


    [Fact]
    public unsafe void TestHookDeleteFileA_MarshalledHP()
    {
        UInt64 fnAddress = (UInt64)GetProcAddress(LoadLibraryA("kernel32.dll"), "DeleteFileA");
        deletefilea_delegate call = Marshal.GetDelegateForFunctionPointer<deletefilea_delegate>((IntPtr)fnAddress);

        // Make sure it works without hooking first
        Assert.False(call("_SHOULD_NOT_EXIST_"));
        UInt64 userTrampVar = 0;
        IntPtr handle = PInvokeInterfaceHP.ph2_x86Detour_create(fnAddress, (UInt64)Marshal.GetFunctionPointerForDelegate<deletefilea_delegate>(TestHookImplMarshalledDeleteFileA), &userTrampVar);

        // Make sure the hook is not yet applied and a detour handle exists
        Assert.False(call("_SHOULD_NOT_EXIST_"));
        Assert.NotEqual(IntPtr.Zero, handle);

        // Make sure is not hooked
        Assert.True(!PInvokeInterfaceHP.ph2_IHook_isHooked(handle).IsTrue());
        Assert.NotEqual(IntPtr.Zero, handle);

        // Make sure the hook does what its supposed to when applied properly.
        Assert.True(PInvokeInterfaceHP.ph2_x86Detour_hook(handle).IsTrue());
        Assert.NotEqual(IntPtr.Zero, handle);

        // Make sure the trampoline works aswell
        Assert.False(Marshal.GetDelegateForFunctionPointer<deletefilea_delegate>((IntPtr)userTrampVar)("_SHOULD_NOT_EXIST_"));
        Assert.NotEqual(IntPtr.Zero, handle);

        // Re-Hook
        Assert.True(PInvokeInterfaceHP.ph2_Detour_reHook(handle).IsTrue());
        Assert.True(call("_SHOULD_NOT_EXIST_"));

        // Make sure the clean up process works aswell
        Assert.True(PInvokeInterfaceHP.ph2_Detour_unHook(handle).IsTrue());
        Assert.False(call("_SHOULD_NOT_EXIST_"));
    }

    public static bool TestHookImplMarshalledDeleteFileA(string lpFileName)
    {
        return true;
    }
}